class User < ActiveRecord::Base

  devise :database_authenticatable, :registerable,
          :validatable
  validates :name, presence: true, length: {maximum: 50}

  has_many :images
  has_many :messages
end
