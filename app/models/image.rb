class Image < ActiveRecord::Base
  has_attached_file :picture,
                    styles: {medium: '400x400>', thumb: '150x150>'},
                    default_url: '/image/:style/missing.png'
  validates_attachment_content_type :picture,
                                    presence: true,
                                    content_type: ['image/jpeg', 'image/png']

  validates :title, presence: true, length: {maximum: 50}

  belongs_to :user
  has_many :messages
end
