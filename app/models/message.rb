class Message < ActiveRecord::Base
  validates :grade, presence: true,
            numericality: { greater_than_or_equal_to: 1, maximum: 5 }
  validates :content, presence: true, length: {maximum: 180 }

  belongs_to :user
  belongs_to :image
end
