ActiveAdmin.register Image do

  permit_params :user_id, :title, :picture

  form do |f|
    f.inputs do
      f.input :title
      f.input :picture, :as => :file, :hint => image_tag(f.object.picture.url(:thumb))
    end
    f.actions
  end

  index do
    selectable_column
    id_column
    column :picture do |image|
      image_tag image.picture.url(:thumb)
    end
    column :title
    actions
  end

  show do
    attributes_table do
      row :image do |image|
        image_tag image.picture.url(:medium)
      end
      row :title
    end
    active_admin_comments
  end
end
