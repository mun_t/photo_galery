ActiveAdmin.register User do

  permit_params :name, :email, :password, :password_confirmation

  form do |f|
    f.inputs do
      f.input :name
      f.input :email
    end
    f.actions
  end

  index do
    selectable_column
    id_column
    column :name
    column :email
    actions
  end

  show do
    attributes_table do
      row :name
      row :email
    end
    active_admin_comments
  end


end
