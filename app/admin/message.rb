ActiveAdmin.register Message do

  permit_params :user_id, :grade, :content

  form do |f|
    f.inputs do
      f.input :grade
      f.input :content
    end
    f.actions
  end

  index do
    selectable_column
    id_column
    column :grade
    column :content
    column :user_id
    column :image_id
    actions
  end

  show do
    attributes_table do
      row :grade
      row :content
      row :user_id
      row :image_id
    end
    active_admin_comments
  end

end
