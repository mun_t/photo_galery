class UsersController < ApplicationController
  before_action :user_signed_in?
  before_action :current_user
  def show
    @user = User.find(params[:id])
    @images = Image.where(@user.id == params[:id])
  end
end
