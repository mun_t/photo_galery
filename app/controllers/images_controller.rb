class ImagesController < ApplicationController
  before_action :current_user, only: [:create, :destroy]
  def new
    @image = Image.new
  end

  def create
    @image = current_user.images.build(image_params)
    if @image.save
      flash[:success] = 'You have created new post!'
      redirect_to root_url
    else
      render 'new'
    end
  end

  def destroy
    @image = Image.destroy(params[:id])
    redirect_to root_url
  end

  def index
    # @images = Image.paginate(:page => params[:page], :per_page => 10)
    @images = Image.all
  end

  def show
    @image = Image.find(params[:id])
    @message = Message.new
    @messages = Message.all.where(@message.image_id == @image.id)
    @messages.order(created_at: 'desc')
    @average_score = get_average_score
  end

  GRADES = [1, 2, 3, 4, 5]


  def create_comment
    if user_signed_in?
      @message = current_user.messages.build(message_params)
      @message.image_id = params[:id]
    end
    if @message.save
      flash[:success] = 'You have created new comment!'
      redirect_to root_url
    else
      render 'new'
    end
  end


  private

  def image_params
    params.require(:Image).permit(:picture, :title, :user_id)
  end

  def message_params
    params.require(:message).permit(:content, :grade, :user_id, :image_id)
  end

  def get_average_score
    average_score = 0
    @messages = Message.where(@message.image_id == params[:id])
    counter = 0
    @messages.each do |message|
      counter += 1
      average_score += message.grade
      average_score/= counter
    end
    return average_score.round(2)
  end

end

