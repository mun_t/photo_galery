
admin_user = User.create(name: 'Admin', email: 'admin@example.com', password: 'adminadmin', password_confirmation: 'adminadmin', admin: true)

fixtures_path = Rails.root.join('app', 'assets', 'images')

users = []
users << User.create(name: 'Jane Doe', email: 'janedoe@example.com', password: 'asdf1111', password_confirmation: 'asdf1111')
users << User.create(name: 'John Doe', email: 'johndoe@example.com', password: '12345678', password_confirmation: '12345678')

images = []
6.times do
  images << Image.create(title: Faker::Lorem.word, picture: File.new(fixtures_path.join('lavender.jpg')), user: users.sample)
end

grades = [1, 2, 3, 4, 5]
5.times do
  Message.create(content: Faker::Lorem.sentences(2), grade: grades.sample, user: users.sample, image: images.sample)
end


