class AddImageToMessages < ActiveRecord::Migration
  def change
    add_reference :messages, :image, index: true, foreign_key: true
  end
end
