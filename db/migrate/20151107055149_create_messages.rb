class CreateMessages < ActiveRecord::Migration
  def change
    create_table :messages do |t|
      t.text :content
      t.decimal :grade

      t.timestamps null: false
    end
  end
end
